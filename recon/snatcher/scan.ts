import 'requests';
import  'sockets';
import { spawn } from 'child_process';
import { Worker } from 'worker_threads';
import { Key } from 'readline';
declare namespace RedTeamTK {
    export class Snatcher{};
    export interface Options< T = Snatcher>{};
}

namespace RedTeamTK {

    export module ServerScanner {
    export type RangeData = {
            rangeStart: Number,
            rangeEnd : Number,
            length : Number
    }
    
 
    export interface BasicRange<T=RangeData> {
        iterCurrent : Number;
        rangeLength : Number;
        range() : RangeData;
    }
    export interface BasicRange<T=number> {
        
    }
    export class BasicRange implements BasicRange<RangeData>{
        private iterCurrent : Number = new Number(0);
        private rangeLength : Number = new Number(1);
        constructor(public start: number, public end: number) {
            this.rangeLength = new Number(this.end - this.start);
        }
        get range() :RangeData {
            let rdata: RangeData = {
                rangeStart: this.start,
                rangeEnd: this.end,
                length: this.rangeLength
            }
            return rdata;
        }
        public next() : number | Error {
            //TODO: Thread safty check
            return (this.iterCurrent.valueOf() + 1 >= this.end) ? Error("End of Range") : this.iterCurrent.valueOf() + 1;
        }
    }
    /**
     *
     *
     * @export
     * @interface Options
     * @Type is Snatcher
     * @description holds the options for the scanner:
     *              - depth: how many discovered directories to traverse - i.e. how far up in this servers, ahem port(?) you gonna get?
     *              - threads: how many threads (max) to use
     *              - wordlist: my "wordlist" is bigger than yours (makes getting taylored suite trousers hard), but as they say its all about targetting
     *              - extensions: if you want hit me with specific extensions your trying to get i.e. .sql, .inc, .dmg, .lic as an array
     *              - mutations: I don't have (super power - gained through powerplant disaster or otherwise) any but some people like to grow numbers or 
     *                           dates on their 'hidden' files to keep them from being found or just to keep track of when they last left their data to be found
     *                           so this option allows you to define a charset, and positions as an array (0 start of string 9999 hopefully the end?!)
     *                           to add in your mutations (extra arms, eyes and legs allowed - superpowers preferred) for example: 
     *                             
     
     */
    //  the date in this example "just so happens" to be my birthday - any gifts welcome samuel.aldis@owasp.org 🎉🎊🎁
    //  I have replaced all 00 with ^ as we will probably use this later as a mutation.....
    DEFAULTS.mutations =  {
        mutation: 
        
    }
    
    export type mutations = {
        mutations:  Array<Function>,
        range: BasicRange
    }
    export type REQUIRED = {
        mutations: mutations;
        wordlist : File | String;
        threads : number;
        depth : number;
        extensions : CallableFunction[];
    }

    export var GLOBAL : REQUIRED = {
        mutations : {
            mutations:  
                    [
                        date = ()=>{
                            var utc = new Date("1991/01/07").toUTCString(); let a = (utc) => { utc = utc.replace("00", "^"); utc = (utc.indexOf("00") >= 0) ? a(utc) : utc; return utc; }; return a(utc) };
                        },
                        literal = () => String(`a-zA-Z!@#$%^1-9$`)
            
                        
                    ]
            }
        }
        range: new BasicRange(0,1000)
        ],
    };
    export interface Options<T = Snatcher> {
        depth: number;
        threads: number;
        wordlist: string;
        extentions?: string[];
        mutations?: [{
            mutation: Object,
            positions: Range
        }]
    }
    
    /**
     *
     *
     * @class Snatcher
     * @description Used to locate backups, hidden directories on a given server, 
     * multi-threaded and configurable as a plugin for other modules
     */
    class Snatcher {
        private options : Options<Snatcher> = {
            depth: 1,
            threads: 5,
            wordlist: "./wordlist.txt",
            mutations : [
                {
                    mutation: Date.now(),
                    positions: BasicRange(0,1000)
                }
            ]
        }
        private taskMaster : Worker;
        
        private workers : Worker[]
        
        constructor(public baseurl : string, opts? : Options<Snatcher>) {
            if(typeof opts !== typeof undefined) {
                this.options = <Options<Snatcher>> opts;
            }
            this.taskMaster.setMaxListeners(this.options.threads);

            this.options.mutations[0].positions.
        }
        public start() {
            
        }
        private async callLoop() {
             spawn('()=>{this.callLoop()}');
        }
        static hooks = {
            error : ()=>eval("throw 'Error'")
        }
    }
    }
}